﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @foreach
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = new string[6];

            names[0] = "Luis A. Sierra";
            names[1] = "Mattew";
            names[2] = "Victoria";
            names[3] = "Josue";
            names[4] = "Kelvin";
            names[5] = "Victor";

            foreach (string name in names)
            {
            	Console.WriteLine("Name: {0}", name);
            }

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_ASCII
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Convert to ASCII");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("ASCII: {0}", Convert.ToChar(65));
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}

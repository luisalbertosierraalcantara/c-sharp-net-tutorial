﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_Integer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Convert to Integer");
            Console.WriteLine("-----------------------------------");
            string text = "15";
            int number = Convert.ToInt16(text);
            Console.WriteLine("Int Number: {0}", Convert.ToString(number));
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}

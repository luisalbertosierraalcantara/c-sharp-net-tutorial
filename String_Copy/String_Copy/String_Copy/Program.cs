﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String_Copy
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("String Copy");
            Console.WriteLine("-----------------------------------");
            string text;
            text = "Charp";
            Console.WriteLine(text);

            char[] chrs;
            chrs = new char[5];

            text.CopyTo(0, chrs, 0, 5);

            Console.WriteLine("Copy: {0}", chrs[3]);

            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}

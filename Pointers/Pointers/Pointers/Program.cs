﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pointers
{
    class Program
    {
        static void Main(string[] args)
        {
            unsafe
            {

                int* a;

                a = (int*) 100;

                Console.WriteLine("Memory Value: {0}", (int) a);     // Displays the value at the memory address.
                Console.WriteLine("Memory Address: {0}", (int) &a);  // Displays the memory address 
                Console.ReadLine();
                
            }

        }
    }
}

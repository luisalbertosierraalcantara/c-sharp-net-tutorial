﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_Date
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Convert to Date");
            Console.WriteLine("-----------------------------------");
            string text = "13/10/2022";
            DateTime _date;
            _date = Convert.ToDateTime(text);
            Console.WriteLine("Date: {0}", _date.ToShortDateString());
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}

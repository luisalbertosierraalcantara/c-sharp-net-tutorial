﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_Time
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Convert to Time");
            Console.WriteLine("-----------------------------------");
            string text = "12:00";
            DateTime _time;
            _time = Convert.ToDateTime(text);
            Console.WriteLine("Time: {0}", Convert.ToString(_time.TimeOfDay));
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}

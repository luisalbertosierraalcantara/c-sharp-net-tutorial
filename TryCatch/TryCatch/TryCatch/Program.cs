﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatch
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int result = 0;
                string x,y;

                Console.WriteLine("Enter the First Number");
                x = Console.ReadLine();

                Console.WriteLine("Enter the Second Number");
                y = Console.ReadLine();

                result = Convert.ToInt16(x) / Convert.ToInt16(y);

                Console.WriteLine("Result: {0}", Convert.ToString(result));

            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("Result: {0}", e.ToString());
            }
            finally
            { 
               //Always stop by here 
            }

            Console.ReadLine();
        }
    }
}

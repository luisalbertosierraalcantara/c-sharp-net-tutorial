﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_DateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Convert to DateTime");
            Console.WriteLine("-----------------------------------");
            string text = "13/10/2022 12:00";
            DateTime _datetime;
            _datetime = Convert.ToDateTime(text);
            Console.WriteLine("DateTime: {0}", Convert.ToString(_datetime));
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}

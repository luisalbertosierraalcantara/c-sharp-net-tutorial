﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace While
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            bool condition = true;

            while (condition)
            {
                if (i == 50) condition = false;

                Console.WriteLine("Number: {0}", Convert.ToString(i));

                i++;
            }

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @for
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;

            for (x = 1; x <= 100; x++)
            {
                Console.WriteLine("Number: {0}", Convert.ToString(x));
            }

            Console.ReadLine();
        }
    }
}

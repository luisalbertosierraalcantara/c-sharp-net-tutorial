﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] phones;                                          // Arrays of strings
            phones = new string[] {"1809-445-5664", "1983-345-3333"}; // assingnment  the array

            string[] names;                                           // Arrays of strings
            names = new string[] {"Luis A. Sierra", "Jose Bancuver"}; // assingnment  the array

            string[] car = new string[3];                             // Arrays of three strings
            car[0] = "Ford";                                          // assingnment the index 0 of the Array

            string[]  Colors = new string[] { "Red", "Blue", "White" }; // definition and assingnment at the same time

            // print in console the index 0
            Console.WriteLine("Array phone: {0}", phones[0]);
            Console.WriteLine("Array name: {0}", names[0]);
            Console.WriteLine("Array car: {0}", car[0]);
            Console.WriteLine("Array color: {0}", Colors[0]);
            Console.ReadLine();
        }
    }
}

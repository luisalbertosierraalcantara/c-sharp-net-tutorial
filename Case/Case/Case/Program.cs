﻿using System;

namespace Case
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 1;

            switch (x)
            {
                case 1:
                    {
                        Console.WriteLine("Selected: {0}", "1");
                        break;
                    }

                case 2:
                    {
                        Console.WriteLine("Selected: {0}", "2");
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Selected: {0}", "3");
                        break;
                    }
                default :
                    {
                        Console.WriteLine("Selected: {0}", "Default");
                        break;
                    }
            }

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Format_DateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Format DateTime");
            Console.WriteLine("-----------------------------------");
            string text = "13/10/2022 12:00";
            DateTime _datetime;
            _datetime = Convert.ToDateTime(text);
            Console.WriteLine("Format: {0}", _datetime.ToString("dddd MMMM yyyy h:mm"));
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertString
{
    class Program
    {
        static void Main(string[] args)
        {
          Console.WriteLine("Convert to String");
          Console.WriteLine("-----------------------------------");
          Console.WriteLine("String Date: {0}", Convert.ToString(DateTime.Now));
          Console.WriteLine("String Number: {0}",  Convert.ToString(67));
          Console.WriteLine("String Decimal: {0}",  Convert.ToString(96.89));
          Console.WriteLine("String Boolean: {0}",  Convert.ToString(true));
          Console.WriteLine(Convert.ToChar(13));
          Console.ReadLine();
        }
    }
}

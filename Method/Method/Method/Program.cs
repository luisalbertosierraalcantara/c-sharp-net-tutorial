﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method
{
    class Program
    {
        static void SUM(int x, int y)
        {
            int result = 0;

            result = x + y;

            Console.WriteLine("SUM: {0}", Convert.ToString(result));
        }

        static void Main(string[] args)
        {
            SUM(5, 5);
            Console.ReadLine();
        }

    }
}

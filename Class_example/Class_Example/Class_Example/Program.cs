﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int mult = 0;

            Class1 Operation = new Class1();  //Instantiate class to operation variable

            sum = Operation.SUM(3, 5);        //Set Funtion Result (SUM) to the sum variable
            mult = Operation.MULT(3, 5);      //Set Funtion Result (MULT) to the mult variable

            Console.WriteLine("SUM: {0}", Convert.ToString(sum));   //Convert to String and print
            Console.WriteLine("MULT: {0}", Convert.ToString(mult)); //Convert to String and print

            Console.ReadLine();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Types
{
    class Program
    {
        static void Main(string[] args)
        {
            int id;
            string name = "Luis A. Sierra";
            double percentage = 18.55;
            char gender = 'M';
            bool isVerified;

            id = 10;
            isVerified = true;

            Console.WriteLine("Id: {0}", id);
            Console.WriteLine("Name: {0}", name);
            Console.WriteLine("Percentage: {0}", percentage);
            Console.WriteLine("Gender: {0}", gender);
            Console.WriteLine("Verfied: {0}", isVerified);
            Console.ReadLine();
        }
    }
}

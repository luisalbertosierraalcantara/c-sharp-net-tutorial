﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_types_convertions
{
    class Program
    {
        static void Main(string[] args)
        {
             string val = "456";

             int parsedVal;

             bool result = Int32.TryParse(val, out parsedVal);

              // With Code block
             if (int.TryParse(val, out parsedVal))
             {
                 Console.WriteLine(parsedVal);
             }
             else
             {
                 Console.WriteLine("ERROR: Parsing Data...");  
             }
 
             Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Property_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 Acc = new Class1();

            Acc.Pro_Account = 1233455; //set  account property

            Console.WriteLine("Account: {0}", Convert.ToString(Acc.Pro_Account)); //Get Property from Class 1

            Console.ReadLine();
        }
    }
}

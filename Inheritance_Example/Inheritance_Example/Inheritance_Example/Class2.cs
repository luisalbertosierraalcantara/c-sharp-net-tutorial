﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance_Example
{
    class Class2 : Class1  //Inherits property form class1
    { 
        
       public void ShowData() {
         
         Pro_Account = 357886; //Set property tu class 1

         //Print data to console
         Console.WriteLine("Account: {0} ", Convert.ToString(Pro_Account)); //Get property tu class 1
         Console.WriteLine("Name: {0} ","Luis A. Sierra");
          
       }

    }
}

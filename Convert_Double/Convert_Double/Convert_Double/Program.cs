﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_Double
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Convert to double");
            Console.WriteLine("-----------------------------------");
            string text = "19.5";
            double number = Convert.ToDouble(text);
            Console.WriteLine("Double Decimal: {0}", Convert.ToString(number));
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}

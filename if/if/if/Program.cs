﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @if
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 34;

            if (age > 18) Console.WriteLine("Age: {0}", "You are older");

            Console.ReadLine();
        }
    }
}

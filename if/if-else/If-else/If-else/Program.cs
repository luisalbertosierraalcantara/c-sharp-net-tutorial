﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace If_else
{
    class Program
    {
        static void Main(string[] args)
        {

            int age = 67;

            if (age < 18)
            {
                Console.WriteLine("Age: {0}", "You are young");
            }
            else
            {
                Console.WriteLine("Age: {0}", "You are older");
            }

            Console.ReadLine();
        }
    }
}

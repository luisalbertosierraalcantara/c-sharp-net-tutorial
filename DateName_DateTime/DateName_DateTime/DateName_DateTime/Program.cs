﻿using System;
namespace DateName_DateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("DayName of DateTime");
            Console.WriteLine("-----------------------------------");
            string DayName = "";
            string text = "13/10/2022 12:00";
            DateTime date_time;
            date_time = Convert.ToDateTime(text);
            int DayNumber = Convert.ToInt16(date_time.DayOfWeek);

            if (DayNumber == 1) 
                DayName = "Sunday";
            else
            if (DayNumber == 2) 
                DayName = "Monday";
            else
            if (DayNumber == 3) 
                DayName = "Tuesday";
            else
            if (DayNumber == 4) 
                DayName = "Wednesday";
            else
            if (DayNumber == 5) 
                DayName = "Thursday";
            else
            if (DayNumber == 6) 
                DayName = "Friday";
            else
            if (DayNumber == 7) 
                DayName = "Saturday";          

            Console.WriteLine("Day: {0}", DayName);
            Console.WriteLine(Convert.ToChar(13));
            Console.ReadLine();
        }
    }
}
